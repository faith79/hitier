package de.faith.hitier.website;

import java.util.List;

import org.openqa.selenium.chrome.ChromeDriver;

import de.faith.hitier.controller.FxmlMainController;

public class Website {

    private final String operationNumber;
    private final String password;
    private final String date;
    private final boolean access;
    private final List<String> earmarks;
    private final ChromeDriver chrome;

    public Website(String operationNumber, String password, String date, boolean access, List<String> earmarks) {
        this.operationNumber = operationNumber;
        this.password = password;
        this.access = access;
        this.earmarks = earmarks;
        this.date = date;
        if (FxmlMainController.getChromeDriver() != null) {
            FxmlMainController.getChromeDriver().quit();
        }
        chrome = new ChromeDriver();
    }

    public void start() {

        chrome.get("http://www3.hi-tier.de/hitcom/login.asp");

        // Login
        chrome.findElementByCssSelector("tbody > tr:first-child > td:nth-of-type(2) > input").sendKeys(this.operationNumber);
        chrome.findElementByCssSelector("#idPasswort").sendKeys(this.password);
        chrome.findElementByCssSelector("#idSubmitLogin").click();

        // Zugang/Abgang
        chrome.findElementByCssSelector("#sm_body > table:nth-of-type(2) > tbody > tr:nth-of-type(2) > td:nth-of-type(2) > a").click();

        for (String earmark : this.earmarks) {
            chrome.findElementByCssSelector("body > table:nth-child(1) > tbody > tr > td > form > table:nth-child(1) > tbody > tr:nth-child(" + 2 + ") > td:nth-child(1) > input").sendKeys(earmark);
            if (this.access) {
                chrome.findElementByCssSelector("tbody > tr:nth-of-type(" + 2 + ") > td:nth-of-type(3) > input.js-datepicker-aktuell-tabelle.hasDatepicker").sendKeys(this.date);
            } else {
                chrome.findElementByCssSelector("tbody > tr:nth-of-type(" + 2 + ") > td:nth-of-type(5) > input.js-datepicker-aktuell-tabelle.hasDatepicker").sendKeys(this.date);
            }
            // Einf�gen
            chrome.findElementByCssSelector("tr > td:nth-of-type(2) > input").click();
            // Neu laden
            chrome.findElementByCssSelector("[name=\"frmBeweg\"] > table:nth-of-type(2) > tbody > tr > td:nth-of-type(3) > input").click();
        }

    }

    public ChromeDriver getChromeDriver() {
        return chrome;
    }
}
