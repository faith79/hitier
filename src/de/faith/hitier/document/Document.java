package de.faith.hitier.document;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class Document {

    private String path;
    private String name;
    private List<String> text;

    public Document(String path, String name, List<String> text) {
        this.path = path;
        this.name = name;
        this.text = text;
    }

    public void createDocument() {
        try {
            if (!path.isEmpty()) {
                XWPFDocument doc = new XWPFDocument();
                FileOutputStream out = new FileOutputStream(new File(path + "/" + name + ".docx"));

                XWPFParagraph paragraph = doc.createParagraph();
                XWPFRun run = paragraph.createRun();

                for (String s : text) {
                    run.setText(s);
                    run.addBreak();
                }
                doc.write(out);
                doc.close();
                out.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Finish: document created");
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getText() {
        return text;
    }

    public void setText(List<String> text) {
        this.text = text;
    }

}
