package de.faith.hitier.controller;

import java.net.URL;
import java.util.ResourceBundle;
import java.time.format.DateTimeFormatter;

import de.faith.hitier.HiTier;
import de.faith.hitier.file.FileManager;
import de.faith.hitier.form.Form;
import de.faith.hitier.website.Website;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class FxmlUploadController implements Initializable {

    private FileManager file = new FileManager();

    @FXML
    private CheckBox checkBox;

    @FXML
    private ComboBox<String> comboBox;

    @FXML
    private TextField operationNumber;

    @FXML
    private TextField password;

    @FXML
    private DatePicker date;

    @FXML
    private Pane headerPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (file.isPasswordSaved()) {
            operationNumber.setText(file.getPassword().split(":")[0]);
            password.setText(file.getPassword().split(":")[1]);
            checkBox.setSelected(true);
        }
        comboBox.getItems().clear();
        comboBox.getItems().addAll("Zugang", "Abgang");
        FxmlMainController.moveHeader(headerPane);
    }

    @FXML
    private void switchScene(ActionEvent e) {
        Stage currentStage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        currentStage.setScene(HiTier.getScene());
        currentStage.show();
    }

    @FXML
    private void startBrowser(ActionEvent e) {
        Stage currentStage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        Form.setOperationNumber(operationNumber.getText());
        Form.setPassword(password.getText());
        if (checkBox.isSelected()) {
            file.savePassword();
        } else {
            file.deletePassword();
        }
        if (comboBox.getValue() != null) {
            Form.setAccess(comboBox.getValue().equals("Zugang"));
        }
        if (date.getValue() != null) {
            Form.setDate(date.getValue().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        }
        if (Form.getOperationNumber() != null && Form.getPassword() != null && Form.getDate() != null && Form.getEarmarks() != null) {
            Website website = new Website(Form.getOperationNumber(), Form.getPassword(), Form.getDate(), Form.isAccess(), Form.getEarmarks());
            website.start();
            FxmlMainController.setChromeDriver(website.getChromeDriver());
        }
        currentStage.setScene(HiTier.getScene());
        currentStage.show();
    }



}
