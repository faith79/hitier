package de.faith.hitier.controller;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import de.faith.hitier.HiTier;
import de.faith.hitier.document.Document;
import de.faith.hitier.form.Form;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class FxmlExportController implements Initializable {

    @FXML
    private TextField name;

    @FXML
    private TextField path;

    @FXML
    private RadioButton openFile;

    @FXML
    private Pane headerPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        FxmlMainController.moveHeader(headerPane);
    }

    @FXML
    private void setPath(ActionEvent e) {
        DirectoryChooser chooser = new DirectoryChooser();
        File selectedDirectory = chooser.showDialog(((Node) e.getSource()).getScene().getWindow());
        chooser.setTitle("Datei Pfad setzen");
        if (selectedDirectory != null) {
            path.setText(selectedDirectory.getAbsolutePath());
        }
    }

    @FXML
    private void onClose(ActionEvent e) {
        Stage currentStage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        currentStage.setScene(HiTier.getScene());
        currentStage.show();
        System.out.println("Zur�ck");
    }

    @FXML
    private void onFinish(ActionEvent e) {
        Document document = new Document(path.getText() + "/", name.getText(), Form.getEarmarks());
        Stage currentStage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        currentStage.setScene(HiTier.getScene());
        currentStage.show();
        document.createDocument();
        System.out.println("Fertig");
    }



}
