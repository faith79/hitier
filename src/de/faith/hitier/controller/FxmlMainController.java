package de.faith.hitier.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.stage.FileChooser;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.openqa.selenium.chrome.ChromeDriver;

import de.faith.hitier.HiTier;
import de.faith.hitier.form.Form;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class FxmlMainController implements Initializable {

    private List<String> barcodes = new ArrayList<>();
    private static ChromeDriver chrome;
    private static double xOffset;
    private static double yOffset;
    private String barcode;

    @FXML
    private TextArea textArea;

    @FXML
    private Pane headerPane;

    @FXML
    private void shutdown() {
        if (chrome != null) {
            chrome.quit();
        }
        System.exit(0);
    }

    @FXML
    private void openFileChooser(ActionEvent e) {
        try {
            Form.setEarmarks(getBarcodes());
            Parent exportPage = FXMLLoader.load(HiTier.class.getResource("HiTierFileChooser.fxml"));
            switchScene(e, exportPage);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        System.out.println("Exportiere in Word Dokument");
    }

    @FXML
    public void importBarcodes(ActionEvent e) {
        FileChooser fileChooser = new FileChooser();
        File file;
        fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Word-Document ", "*.docx"));
        file = fileChooser.showOpenDialog(((Node) e.getSource()).getScene().getWindow());
        if (file != null) {
            try {
                FileInputStream inputStream = new FileInputStream(file.getAbsolutePath());
                XWPFDocument document = new XWPFDocument(inputStream);
                List<XWPFParagraph> paragraphs = document.getParagraphs();
                StringBuilder barcodes = new StringBuilder();

                for (XWPFParagraph para : paragraphs) {
                    for (String barcode : para.getText().split("\\s")) {
                        if (barcode.length() == 12) {
                            this.barcodes.add(barcode);
                        }
                    }
                }
                for (String barcode : this.barcodes) {
                    barcodes.append(barcode).append("\n");
                }

                textArea.setText(barcodes.toString());
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
    }

    @FXML
    private void openHiTierUpload(ActionEvent e) {
        try {
            Form.setEarmarks(getBarcodes());
            Parent uploadPage = FXMLLoader.load(HiTier.class.getResource("HiTierUpload.fxml"));
            switchScene(e, uploadPage);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    private void switchScene(ActionEvent e, Parent parent) {
        Scene scene = new Scene(parent);
        Stage currentStage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        currentStage.setScene(scene);
        currentStage.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        onKeyPressed();
        moveHeader(headerPane);
    }

    private void onKeyPressed() {
        Thread thread = new Thread(() -> {
            try {
                String finishList = "";
                while (true) {
                    String[] lines = textArea.getText().split("\\n");
                    for (String line : lines) {
                        String s = line;
                        if (s.length() == 16) {
                            if (s.startsWith("27600")) {
                                s = s.replace("27600", "DE");
                                s = s.substring(0, s.length() - 1);
                                finishList += s + "\n";
                                if (finishList.length() != 0) {
                                    barcode = finishList;
                                    StringBuilder barcodes = new StringBuilder();
                                    for (String codes : getBarcodes()) {
                                        barcodes.append(codes).append("\n");
                                    }
                                    textArea.setText(barcodes.toString());
                                    textArea.setScrollTop(Double.MAX_VALUE);
                                    System.out.println("richtiger barcode");
                                }
                            } else {
                                System.err.println("Falscher Barcode 1 f�ngt nicht mit DE an");
                                StringBuilder barcodes = new StringBuilder();
                                for (String codes : getBarcodes()) {
                                    barcodes.append(codes).append("\n");
                                }
                                textArea.setText(barcodes.toString());

                            }
                        } else {
                            if (!s.isEmpty() && !s.startsWith("27600") && !s.startsWith("DE")) {
                                StringBuilder barcodes = new StringBuilder();
                                for (String codes : getBarcodes()) {
                                    barcodes.append(codes).append("\n");
                                }
                                textArea.setText(barcodes.toString());
                                System.err.println("Falscher Barcode gescannt");
                            }
                        }
                    }
                    Thread.sleep(100);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        thread.start();
    }


    static void moveHeader(Pane headerPane) {
        headerPane.setOnMousePressed(event -> {
            xOffset = HiTier.getStage().getX() - event.getScreenX();
            yOffset = HiTier.getStage().getY() - event.getScreenY();
        });
        headerPane.setOnMouseDragged(event -> {
            HiTier.getStage().setX(event.getScreenX() + xOffset);
            HiTier.getStage().setY(event.getScreenY() + yOffset);
        });
    }

    private List<String> getBarcodes() {
        if (barcode != null) {
            String[] lines = barcode.split("\\r?\\n");
            for (String s : lines) {
                if (!barcodes.contains(s)) {
                    barcodes.add(s);
                }
            }
        }
        return barcodes;
    }

    static void setChromeDriver(ChromeDriver chrome) {
        FxmlMainController.chrome = chrome;
    }

    public static ChromeDriver getChromeDriver() {
        return chrome;
    }
}
