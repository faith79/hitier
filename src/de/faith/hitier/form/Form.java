package de.faith.hitier.form;

import java.util.ArrayList;
import java.util.List;

public class Form {

    private static List<String> earmarks = new ArrayList<>();
    private static String operationNumber;
    private static String password;
    private static String date;
    private static boolean access;

    public static List<String> getEarmarks() {
        return earmarks;
    }

    public static void setEarmarks(List<String> earmarks) {
        Form.earmarks = earmarks;
    }

    public static String getOperationNumber() {
        return operationNumber;
    }

    public static void setOperationNumber(String operationNumber) {
        Form.operationNumber = operationNumber;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        Form.password = password;
    }

    public static String getDate() {
        return date;
    }

    public static void setDate(String date) {
        Form.date = date;
    }

    public static boolean isAccess() {
        return access;
    }

    public static void setAccess(boolean access) {
        Form.access = access;
    }

}
