package de.faith.hitier.encoding;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class Encoding {

    public String getEncryptedString(String text) {
        byte[] encrypted = null;
        try {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, getSecretKey());
            encrypted = cipher.doFinal(text.getBytes());
        } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
        assert encrypted != null;
        return new String(encrypted);
    }

    public String getDecryptedString(String text) {
        try {
            Cipher cipher2 = Cipher.getInstance("AES");
            cipher2.init(Cipher.DECRYPT_MODE, getSecretKey());
            byte[] cipherData2 = cipher2.doFinal(text.getBytes());
            text = new String(cipherData2);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
        return text;
    }

    private SecretKeySpec getSecretKey() {
        byte[] key = null;
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-256");
            String keyStr = "VlRCV1drMXRUbGRSVkVaT1RVaG9XbFZXVW10bFYxWnlZMGhLYVUwd2NGaFdiRlUxVWtaV1ZVMUVhejA9";
            key = (keyStr).getBytes(StandardCharsets.UTF_8);
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        assert key != null;
        return new SecretKeySpec(key, "AES");
    }
}
