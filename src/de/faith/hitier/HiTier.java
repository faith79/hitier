package de.faith.hitier;

import java.io.File;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class HiTier extends Application {

    private static Scene scene;
    private static Stage stage;

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(HiTier.class.getResource("HiTier.fxml"));
        stage.initStyle(StageStyle.UNDECORATED);
        stage.getIcons().add(new Image("de/faith/hitier/assets/HiTier.jpg"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        setStage(stage);
    }

    public static void main(String[] args) {
        onStart();
        launch(args);
    }

    private static void onStart() {
        System.setProperty("webdriver.chrome.driver", System.getenv("APPDATA") + "\\HiTier\\chromedriver.exe");
        File directory = new File(System.getenv("APPDATA") + "\\HiTier");
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }

    public static Scene getScene() {
        return scene;
    }

    public static Stage getStage() {
        return stage;
    }

    private static void setStage(Stage stage) {
        HiTier.stage = stage;
    }

}
