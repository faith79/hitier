package de.faith.hitier.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import de.faith.hitier.encoding.Encoding;
import de.faith.hitier.form.Form;

public class FileManager {

    private String fileName = "jdkljadikalncaasdi.faith";
    private String filePath = System.getenv("APPDATA") + "\\HiTier";

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void savePassword() {
        try {
            File f = new File(filePath, fileName);
            if (f.exists()) {
                f.delete();
            }
            PrintWriter output = new PrintWriter(new FileWriter(f, true));
            output.println(new Encoding().getEncryptedString(Form.getOperationNumber() + ":" + Form.getPassword()));
            output.close();

            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getPassword() {
        String password = null;
        File f = new File(filePath, fileName);
        if (f.exists()) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(f));
                password = new Encoding().getDecryptedString(br.readLine());
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return password;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void deletePassword() {
        File f = new File(filePath, fileName);
        f.delete();
    }

    public boolean isPasswordSaved() {
        File f = new File(filePath, fileName);
        try {
            if (f.exists()) {
                BufferedReader br = new BufferedReader(new FileReader(f));
                if (!br.readLine().isEmpty()) {
                    br.close();
                    return true;
                }
                br.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

}
